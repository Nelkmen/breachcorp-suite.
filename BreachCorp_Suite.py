#!/usr/bin/env python3
import urllib.request
import urllib.parse
import logging
import urllib3
import pyhibp
import time
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
logging.basicConfig(filename='info.log', encoding='utf-8', level=logging.DEBUG)
import time
import transliterate
from bs4 import BeautifulSoup
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
import ssl
ssl._create_default_https_context = ssl._create_unverified_context
import sys
import re
import socket
from copy import copy

url_google = []
domainlist = []
emails_found = []
visited_links = []

if len(sys.argv) != 3:
    print("python BreachCorp_Suite.py [URL] [Wordlist]")
    sys.exit(1)


def generate_mails(fullnames, domain):
    """
    Description:
    fullnames[] must me in format: ["John_Doe", "Lucas_Ocampos"]
    domain must be in format: "microsoft.com"
    """
    mails = []
    for fn in fullnames:
        fn_arr = fn.lower().split('_')
        name, surname = fn_arr[0], fn_arr[1]
        mails.append(name + surname + '@' + domain)
        mails.append(name + '.' + surname + '@' + domain)
        mails.append(name[0] + '.' + surname + '@' + domain)

    return mails


def check_breach(mails):
    HIBP_API_KEY = "5d0d45510f43481f94544922226e1c5e"
    pyhibp.set_api_key(key=HIBP_API_KEY)
    pyhibp.set_user_agent(ua="NATO Breach Identifier/0.0.1")

    all_breaches = {}
    all_pastes = {}

    for mail in mails:
        pastes = pyhibp.get_pastes(email_address=mail)
        breaches = pyhibp.get_account_breaches(account=mail, truncate_response=True)
        all_breaches[mail] = breaches
        all_pastes[mail] = pastes
        time.sleep(3)  # for API rate

    results = [all_breaches, all_pastes]

    return results


def print_results(results):
    print("[+] Printing results")
    for key, value in results[0].items():
        if value:
            print("[+] Found breach:", key, "-", value)
    for key, value in results[1].items():
        if value:
            print("[+] Found paste:", key, "-", value)
    print("[+] No more results!")


def crawl_subdomains(subdomains):
    for subdomain in subdomains:
        discovered_links = [subdomain]

        while discovered_links:
            if len(visited_links) > 100:
                # If visited more than 100 links, break, a lot of time gone
                break
            try:
                # Display status and mark current link as visited
                print(
                    f'\rLinks to discover - {len(discovered_links)}. Already visited - {len(visited_links)}. Emails found - {emails_found}',
                    end='')
                visited_links.append(discovered_links[0])

                # Discover page content
                request = requests.get(discovered_links[0], verify=False)
                soup = BeautifulSoup(request.text, "html.parser")

                discovered_links.pop(0)

                # ALERT! REDESIGN EMAIL ENDING
                email_pattern = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'

                # Extract same url links from main page
                # It must contain same subdomain
                for link in soup.findAll('a'):
                    found_link = link.get('href')
                    if found_link:
                        if found_link.startswith('/'):
                            found_link = subdomain + found_link

                        if subdomain in found_link and found_link not in visited_links:
                            if 'mailto' in found_link:
                                potential_email = re.findall(email_pattern, found_link)
                                if potential_email and potential_email not in emails_found:
                                    emails_found.append(potential_email[0])
                            else:
                                if found_link not in discovered_links:
                                    discovered_links.append(found_link)

                # Look for email in the page content
                discovered_emails = re.findall(email_pattern, request.text)
                for email in discovered_emails:
                    if email not in emails_found:
                        emails_found.append(email)

            except Exception:
                # Mne tak poyebat, really
                discovered_links.pop(0)
                pass


def google_search(emails, pages_amount):
    emails_found = []

    # Function receives list of emails to extract their corporate subdomains
    email_subdomains = []

    # Extracting subdomain part
    for email in emails:
        email_subdomains.append(email[email.find("@") + 1:])

    # Leaving only unique subdomains
    email_subdomains = set(email_subdomains)

    # Begin search with every subdomain
    for email_subdomain in email_subdomains:
        # In order to prevent block, we have to set real user-agent
        headers = {
            'User-agent':
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.19582'
        }

        # Google search query with dork (corporate subdomain)
        for page in range(0, int(pages_amount * 10), 10):
            try:
                params = {
                    'q': 'intext:@' + email_subdomain,
                    'start': str(page),
                    'filter': '0'
                }

                # Making request
                google_request = requests.get('https://www.google.com/search', headers=headers, params=params)
                soup = BeautifulSoup(google_request.text, 'html.parser')

                # Extracting links from google search result
                discovered_links = []

                for result in soup.select('.tF2Cxc'):
                    link = result.select_one('.yuRUbf a')['href']
                    discovered_links.append(link)

                # Extracting emails from pages (discovered links)
                email_pattern = r'\b[A-Za-z0-9._%+-]+@' + email_subdomain
                for link in discovered_links:
                    request = requests.get(link, verify=False)

                    discovered_emails = re.findall(email_pattern, request.text)
                    for email in discovered_emails:
                        if email not in emails_found:
                            emails_found.append(email)
                            print("[+] Discovered email - " + email)

            except Exception:
                # Mne tak poyebat, really
                continue

    return emails_found


def SearchGoogle(target):
    start_page = 0
    nlink = ""
    num = 5
    user_agent = {'User-agent': 'Mozilla/5.0'}
    nlink_clean = ""
    response = ""
    soup = ""
    raw_links = ""
    # Split the target in domain and extension
    domain = target.replace(".es", '')
    extension = target.split(".")[1]
    print("\nLooking domains and subdomains of target", target)
    for start in range(start_page, (start_page + num)):
        SearchGoogle = "https://www.google.com/search?q=(site:*." + target + "+OR+site:*" + target + "+OR+site:" + domain + "*." + extension + ")+-site:www." + target + "&filter=&num=100"
    try:
        response = requests.get(SearchGoogle, headers=user_agent)
    except requests.exceptions.RequestException as e:
        print("\nError connection to server!")
        pass
    except requests.exceptions.ConnectTimeout as e:
        print("\nError Timeout", target)
        pass
    try:
        # Parser HTML of BeautifulSoup
        soup = BeautifulSoup(response.text, "html.parser")
        if response.text.find("Our systems have detected unusual traffic") != -1:
            print("CAPTCHA detected - Plata or captcha !!!Maybe try form another IP...")
            return True
        # Parser url's throught regular expression
        raw_links = soup.find_all("a", href=re.compile("(?<=/url\?q=)(htt.*://.*)"))
        # print raw_links
        for link in raw_links:
            # Cache Google
            if link["href"].find("webcache.googleusercontent.com") == -1:
                nlink = link["href"].replace("/url?q=", "")
            # Parser links
            nlink = re.sub(r'&sa=.*', "", nlink)
            nlink = urllib.parse.unquote(nlink)
            nlink_clean = nlink.split("//")[-1].split("/")[0]
            url_google.append(nlink_clean)
            domainlist.append(nlink_clean)
    except Exception as e:
        print(e)
    if len(raw_links) < 2:
        # Verify if the search has taken some results
        print("No more results!!!")
        # captcha = True
        return True
    else:
        return False


def get_employees(company_domain, pages_amount):
    """
    :param company_domain: Specify main domain company uses
    :param pages_amount: Specify amount of pages to scrap during Google search
    :return: list of employee names
    """

    # In order not to get blocked set realistic User-Agent
    headers = {
        'User-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.19582'
    }

    # Search query
    params = {
        'q': 'inurl:linkedin "' + company_domain + '"'
    }

    # Performing request to retrieve company name
    company_name_request = requests.get('https://www.google.com/search', headers=headers, params=params)
    soup = BeautifulSoup(company_name_request.text, 'html.parser')

    # Parsing raw title from LinkedIn page
    if soup.select('h3.LC20lb'):
        raw_title = soup.select('h3.LC20lb')[0].text.strip()
    else:
        print('It seems, you\'ve been blocked by Google :(')
        exit()

    # Extracting company name from LinkedIn title
    if '- LinkedIn' in raw_title:
        company_name = raw_title.split(' - LinkedIn')[0]
    elif '| LinkedIn' in raw_title:
        company_name = raw_title.split(' | LinkedIn')[0]
    else:
        company_name = raw_title

    print(f'Extracting employees for company {company_name}...')

    # Performing requests to extract employees
    employees = []

    for page in range(0, int(pages_amount * 10), 10):
        params = {
            'q': 'inurl:linkedin "' + company_name + '"',
            'start': str(page),
            'filter': '0'
        }

        # Making request
        employee_request = requests.get('https://www.google.com/search', headers=headers, params=params)
        soup = BeautifulSoup(employee_request.text, 'html.parser')

        name_pattern_en = r'[A-Z][a-z]* [A-Z][a-z]* \-'
        name_pattern_ru = r'[А-Я][а-я]* [А-Я][а-я]* \-'

        for employee in soup.select('h3.LC20lb'):
            if re.search(name_pattern_en, employee.text.strip()):
                employee_name = re.findall(name_pattern_en, employee.text.strip())[0][:-2].replace(" ", "_")
                employees.append(employee_name)
                print(f'[+] {employee_name}')
            elif re.search(name_pattern_ru, employee.text.strip()):
                employee_name = re.findall(name_pattern_ru, employee.text.strip())[0][:-2]
                transliterated_name = transliterate.translit(employee_name, reversed=True)
                employees.append(transliterated_name)
                print(f'[+] {employee_name} -> {transliterated_name}')
            else:
                continue

    # Leave only unique names
    employees = set(employees)
    return employees


def brute():
    for word in open(sys.argv[2], "r").readlines():
        try:
            dom = word[:-1] + "." + sys.argv[1]
            # print('trying: ' + dom)
            result = socket.getaddrinfo(dom, None, 0, socket.SOCK_STREAM)
            # print("\n\033[1;35m[OK]\033[1;39m", [x[4][0] for x in result][0], "\033[1;35m :\033[1;39m ", dom)
            print(dom)
            domainlist.append(dom)
        except(socket.gaierror) as msg:
            pass


SearchGoogle(sys.argv[1])
brute()
# SearchGoogle(sys.argv[1])
domainlist = list(set(domainlist))

for i in domainlist:
    if sys.argv[1] not in i:
        domainlist.remove(i)
    else:
        print(i + "\n")
for i in domainlist:
    crawl_subdomains(['https://' + i])

print('\nSearching in Google, first 10 pages...')

for email in google_search(emails_found, 10):
    if email not in emails_found:
        emails_found.append(email)
print('Personal mails:\n')
for email in emails_found:
    print(email + '\n')

employees = get_employees(sys.argv[1], 5)

# Test data
corporate_mails = generate_mails(employees, sys.argv[1])
print('Corporate mails:\n')
for email in corporate_mails:
    print(email + '\n')
#print("Corporate mails:", corporate_mails)

print_results(check_breach(corporate_mails))
print_results(check_breach(emails_found))